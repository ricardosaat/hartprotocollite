﻿using System.IO.Ports;
using SerialPortWindowsAPI;

namespace Finaltec.Communication.HartLite
{

    internal class SerialPortWrapperWinApi : ISerialPortWrapper
    {
        private readonly SerialPortWinAPI _serialPort;
        private bool useRTSToggle = true; 

        public SerialPortWrapperWinApi(string comPort, int baudrate, System.IO.Ports.Parity parity, int dataBits, System.IO.Ports.StopBits stopBits)
        {
            _serialPort = new SerialPortWinAPI(comPort, baudrate, parity, dataBits, stopBits);
            _serialPort.DataReceived += _serialPort_ReceivedEvent;    
        }


        public void Open()
        {
            _serialPort.Open();
        }

        public void Close()
        {
            _serialPort.Close();
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            return _serialPort.Read(buffer, offset, count);
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            var bytesToRead = BytesToRead;
            
            if (bytesToRead>0)
            {
               var temp = new byte[bytesToRead];
                Read(temp, 0, bytesToRead);
            }

            _serialPort.Write(buffer, offset, count);
        }

        public int BytesToRead
        {
            get { return _serialPort.BytesToRead; }
        }

        public bool DtrEnable
        {
            get { return _serialPort.DtrEnable; }
            set {
                if (!useRTSToggle)
                {
                    _serialPort.DtrEnable = value;
                }
            }
        }

        public bool RtsEnable
        {
            get { return _serialPort.RtsEnable; }
            set
            {
                if (!useRTSToggle)
                {
                    _serialPort.RtsEnable = value;
                }
            }
        }

        public bool CtsHolding
        {
            get { return _serialPort.CtsHolding; }
        }

        public bool CDHolding
        {
            get { return _serialPort.CDHolding; }
        }

        public string PortName
        {
            get { return _serialPort.PortName; }
            set { _serialPort.PortName = value; }
        }

        private SerialDataReceivedEventHandler _dataReceivedEvent;

        public event SerialDataReceivedEventHandler DataReceived
        {
            add
            {
                _dataReceivedEvent += value;

            }
            remove
            {
                if (_dataReceivedEvent != null)
                {
                    _dataReceivedEvent -= value;
                }
            }
        }

        private void _serialPort_ReceivedEvent(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (_dataReceivedEvent != null)
            {
                _dataReceivedEvent(sender, null);
            }
        }

        public event SerialPinChangedEventHandler PinChanged
        {
            add
            {

            }
            remove
            {

            }
        }
    }
}
