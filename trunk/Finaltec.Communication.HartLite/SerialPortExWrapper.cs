﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using SerialPortWindowsAPI;

namespace Finaltec.Communication.HartLite
{
    internal class SerialPortExWrapper : ISerialPortWrapper
    {
        private readonly SerialPortEx _serialPort;

        public SerialPortExWrapper(string comPort, int baudrate, Parity parity, int dataBits, StopBits stopBits)
        {
            _serialPort = new SerialPortEx(comPort, baudrate, parity, dataBits, stopBits);
        }

        public void Open()
        {
            _serialPort.Open();
            _serialPort.DtrEnable = true;
            _serialPort.SetRtsControlToggle();

        }

        public void Close()
        {
            _serialPort.DtrEnable=false;
            _serialPort.Close();
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            return _serialPort.Read(buffer, offset, count);
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            if (_serialPort.CtsHolding)
            {
                Thread.Sleep(100);
            }
            _serialPort.Write(buffer, offset, count);
        }

        public int BytesToRead
        {
            get { return _serialPort.BytesToRead; }
        }

        public bool DtrEnable
        {
            get { return _serialPort.DtrEnable; }
            set { }
        }

        public bool RtsEnable
        {
            get { return _serialPort.RtsEnable; }
            set {  }
        }

        public bool CtsHolding
        {
            get { return _serialPort.CtsHolding; }
        }

        public bool CDHolding
        {
            get { return _serialPort.CDHolding; }
        }

        public string PortName
        {
            get { return _serialPort.PortName; }
            set { _serialPort.PortName = value; }
        }

        public event SerialDataReceivedEventHandler DataReceived
        {
            add { _serialPort.DataReceived += value; }
            remove { _serialPort.DataReceived -= value; }
        }

        public event SerialPinChangedEventHandler PinChanged
        {
            add { _serialPort.PinChanged += value; }
            remove { _serialPort.PinChanged -= value; }
        }
    }
}
