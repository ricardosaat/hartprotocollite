﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using NUnit.Framework;

namespace SerialPortWindowsAPI.UnitTest
{
    [TestFixture]
    public class SerialPortWinAPITests
    {
        private SerialPortWinAPI _serial;
        const string COMPORT = "COM6";

        static SerialPortWinAPITests()
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo("Finaltec.Communication.HartLite.UnitTest.log4net"));
        }

        [TestFixtureSetUp]
        public void Init()
        {
            _serial = new SerialPortWinAPI(COMPORT, 1200, Parity.Odd, 8, StopBits.One);
            _serial.Open();        
        }

        [TestFixtureTearDown]
        public void Cleanup()
        {
           _serial.Close(); 
        }


        [Test]
        public void TestEventFlag()
        {
            var flagEvent = CommEventFlags.CTS | CommEventFlags.RXCHAR;

            Assert.That(((flagEvent & CommEventFlags.CTS) == CommEventFlags.CTS), Is.True);
        }


        [Test]
        public void SendZeroCommand()
        {
            //send and zero command 
            // ff ff ff ff ff 02 80 00 00 82 
            var buffer = CreateZeroCommand();

            var numberBytesRead = 0;
            var readBytes = new List<int>();
         
            _serial.DataReceived += (sender, args) =>
                                    {
                                        //Read 
                                        var temBuffer = new byte[10];
                                        numberBytesRead = _serial.Read(temBuffer, 0, temBuffer.Length);
                                        if (numberBytesRead>0)
                                        {
                                            for (int i = 0; i < numberBytesRead; i++)
                                            {
                                                readBytes.Add(temBuffer[i]);    
                                            }

                                           
                                             
                                        }
                                    };


            _serial.Write(buffer, 0, 10);
            Thread.Sleep(2000);

            Assert.That(readBytes.Count, Is.GreaterThan(0));
            Console.WriteLine("numberBytesRead:" + readBytes.Count.ToString());
           
        }

        private byte[] CreateZeroCommand()
        {
            var buffer = new byte[10];
            buffer[0] = 0xff;
            buffer[1] = 0xff;
            buffer[2] = 0xff;
            buffer[3] = 0xff;
            buffer[4] = 0xff;
            buffer[5] = 0x02;
            buffer[6] = 0x80;
            buffer[7] = 0x00;
            buffer[8] = 0x00;
            buffer[9] = 0x82;
            return buffer;
        }
    }
}
