﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace Finaltec.Communication.HartLite.UnitTest._HartCommunicationLite
{
    [TestFixture, Category("Manual")]
    class TestUniversalCommands
    {
        private HartCommunicationLite _communication;

        static TestUniversalCommands()
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo("Finaltec.Communication.HartLite.UnitTest.log4net"));
        }


        [TestFixtureSetUp]
        public void Init()
        {
            bool autoZeroCommand = true;
 
            _communication = new HartCommunicationLite("COM4")
                                 {
                                     AutomaticZeroCommand = autoZeroCommand
                                 };
            _communication.PreambleLength = 10;
            //_communication.SwitchAddressTo(new ShortAddress(0));
            _communication.Timeout = new TimeSpan(0,0,0,0,2000);

           

            OpenResult openResult = _communication.Open();
            Assert.That(openResult, Is.EqualTo(OpenResult.Opened));

            if(!autoZeroCommand)
            {
                _communication.SendZeroCommand();
            }

         
        }

        [TestFixtureTearDown]
        public void Cleanup()
        {
            _communication.Close(); 
        }

        [Test]
        public void Command_00_ReadUniqueIdentifier()
        {
            for (int i = 0; i < 1; i++)
            {
              CommandResult commandResult = _communication.SendZeroCommand();

               Assert.That(commandResult, Is.Not.Null);
               Assert.That(commandResult.CommandNumber, Is.EqualTo(0));

               PrintData(commandResult.Data);  
                           
            }
            
        }

        [Test]
        public void Command_01_ReadPrimaryVariable()
        {
            CommandResult commandResult=null;

            for (int i = 0; i < 2; i++)
            {
               
              commandResult = _communication.Send(1);

              Assert.That(commandResult, Is.Not.Null);
              Assert.That(commandResult.CommandNumber, Is.EqualTo(1));  
            }
            

            float value = BytesTofloat(1,commandResult.Data);
            Console.WriteLine("Unit:" + commandResult.Data[0]);
            Console.WriteLine("PrimaryValues:" + value.ToString());
            PrintData(commandResult.Data);
        }

        [Test]
        public void Command_02_ReadPrimaryVariableCurrenteAndPercentOfRange()
        {
            CommandResult commandResult = _communication.Send(2);

            Assert.That(commandResult, Is.Not.Null);
            Assert.That(commandResult.CommandNumber, Is.EqualTo(2));

            Console.WriteLine("Current:" + BytesTofloat(0, commandResult.Data));
            Console.WriteLine("PercentRange:" + BytesTofloat(4, commandResult.Data));
            PrintData(commandResult.Data);
        }

        [Test]
        public void Command_03_ReadDynamicVariablesAndPVCurrent()
        {
            CommandResult commandResult = _communication.Send(3);

            Assert.That(commandResult, Is.Not.Null);
            Assert.That(commandResult.CommandNumber, Is.EqualTo(3));

            Console.WriteLine("Current:" + BytesTofloat(0, commandResult.Data));
            Console.WriteLine("PV:" + BytesTofloat(5, commandResult.Data));
            Console.WriteLine("SV:" + BytesTofloat(10, commandResult.Data));
            //Console.WriteLine("TV:" + BytesTofloat(15, commandResult.Data));
            //Console.WriteLine("4thV:" + BytesTofloat(20, commandResult.Data));
            PrintData(commandResult.Data);
        }

        [Test]
        public void Command_12_ReadMessage()
        {
            CommandResult commandResult = _communication.Send(12);

            Assert.That(commandResult, Is.Not.Null);
            Assert.That(commandResult.CommandNumber, Is.EqualTo(12));

            Console.WriteLine("Message:" + System.Text.Encoding.UTF8.GetString(commandResult.Data, 0, commandResult.Data.Length));
            Console.WriteLine("");
            PrintData(commandResult.Data);
        }

        /// <summary>
        ///    Convertes Bytes para float 
        /// </summary>
        /// <param name="startIntex">local onde está os bytes no vetor</param>
        /// <param name="dataBytes">Vetor com os dados</param>
        /// <returns></returns>
        private float BytesTofloat(int startIntex , byte[] dataBytes )
        {
            byte[] bytes = new byte[4];
            Array.Copy(dataBytes, startIntex, bytes,0, 4);
            if (BitConverter.IsLittleEndian) { Array.Reverse(bytes); }
            return BitConverter.ToSingle(bytes, 0);
        }

        private void PrintData(byte[] bytes)
        {
            for (int i = 0; i < bytes.Length; i++)
            {
                Console.WriteLine("#{0} = {1}" , i, bytes[i]);
            }
            
        }

    }
}
