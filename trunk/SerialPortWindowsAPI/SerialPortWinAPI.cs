﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.IO.Ports;
using System.Threading;
using log4net;

namespace SerialPortWindowsAPI
{
    public class SerialPortWinAPI
    {

        private CommAPI _api;
        private IntPtr _hComHandle;
        private SerialDataReceivedEventHandler _dataReceivedEvent;
        private int _baudrate;
        private Parity _parity;
        private int _dataBits;
        private StopBits _stopBits;
        private string _comPort;
        private DCB _dcb = new DCB();
        private IntPtr txOverlapped = IntPtr.Zero;
        private IntPtr rxOverlapped = IntPtr.Zero;
        private static readonly ILog Log = LogManager.GetLogger(typeof(SerialPortWinAPI));

        public SerialPortWinAPI(string comPort, int baudrate, System.IO.Ports.Parity parity, int dataBits, System.IO.Ports.StopBits stopBits)
        {
            _comPort = comPort;
            _stopBits = stopBits;
            _dataBits = dataBits;
            _parity = parity;
            _baudrate = baudrate;
            _api = new WinCommAPI();
            _hComHandle = new IntPtr(CommAPI.INVALID_HANDLE_VALUE);

        }

        #region Open And Close
        
        public void Open()
        {

            if (CommAPI.FullFramework)
            {
                // set up the overlapped tx IO
                var o = new OVERLAPPED();
                txOverlapped = LocalAlloc(0x40, Marshal.SizeOf(o));
                o.Offset = 0;
                o.OffsetHigh = 0;
                o.hEvent = IntPtr.Zero;
                Marshal.StructureToPtr(o, txOverlapped, true);

                // set up the overlapped rx IO
                o = new OVERLAPPED();
                rxOverlapped = LocalAlloc(0x40, Marshal.SizeOf(o));
                o.Offset = 0;
                o.OffsetHigh = 0;
                o.hEvent = IntPtr.Zero;
                Marshal.StructureToPtr(o, rxOverlapped, true);
            }

            _hComHandle = _api.CreateFile(_comPort);


            if (_hComHandle.ToInt32() == CommAPI.INVALID_HANDLE_VALUE)
            {
                throw new ArgumentException("Error open com port. " + _comPort, "comPort");
            }

            CheckTrueReturn(
              _api.SetCommMask(
              _hComHandle, // handle of communications device
               CommEventFlags.RXCHAR)// size of input buffer
                          ,
              "Error SetCommMask"
           );

            CheckTrueReturn(
                _api.SetupComm(
                _hComHandle, // handle of communications device
                2048, // size of input buffer
                2048 // size of output buffer
                                )
                            ,
                "Error SetupComm"
             );

            CheckTrueReturn(
                _api.PurgeComm(_hComHandle,
                               (uint)
                               (PurgeFlags.PURGE_RXABORT | PurgeFlags.PURGE_RXCLEAR | PurgeFlags.PURGE_TXABORT |
                                PurgeFlags.PURGE_TXCLEAR))
                , "Error PurgeComm");


            // set the Comm timeouts
            CommTimeouts ct = new CommTimeouts();

            // reading we'll return immediately
            // this doesn't seem to work as documented
            ct.ReadIntervalTimeout = uint.MaxValue; // this = 0xffffffff
            ct.ReadTotalTimeoutConstant = 1000;
            ct.ReadTotalTimeoutMultiplier = 0;
            ct.WriteTotalTimeoutConstant = 1000;
            ct.WriteTotalTimeoutMultiplier = 0;

            CheckTrueReturn(
                _api.SetCommTimeouts(_hComHandle, ct),
                "Error SetCommTimeouts");


            SetComState();


            DtrEnable = true;

            _tDataReceived = new Thread(DataReceivedWait);
            _tDataReceived.Name = "WaitingRxThread";
            _tDataReceived.Priority = ThreadPriority.Normal;
            _tDataReceived.Start();


        }

        private void SetComState()
        {

            CheckTrueReturn(_api.GetCommState(_hComHandle, _dcb),
            "_api.GetCommState"
                );

        

            // transfer the port settings to a DCB structure
            _dcb.BaudRate = (uint) _baudrate;
            _dcb.ByteSize = (byte) _dataBits;


            _dcb.fParity = false;
            //None = 0,
            //Odd = 1,
            //Even = 2,
            //Mark = 3,
            //Space = 4,
            _dcb.Parity = (byte)_parity;


          
            //One = 0,
            //One5 = 1,
            //Two = 2

            switch (_stopBits)
            {
                case StopBits.One:
                    _dcb.StopBits = 0;
                    break;

                case StopBits.OnePointFive:
                    _dcb.StopBits = 1;
                    break;

                case StopBits.Two:
                    _dcb.StopBits = 2;
                    break;
            }

            _dcb.fBinary = true;
            _dcb.fDtrControl = DCB.DtrControlFlags.Enable;
            _dcb.fRtsControl = DCB.RtsControlFlags.Toggle;

           _dcb.EofChar = (sbyte) 0;
            _dcb.ErrorChar = (sbyte) 0;
            _dcb.EvtChar = (sbyte) 0;
            _dcb.fAbortOnError = false;
            _dcb.fDsrSensitivity = false;
            _dcb.fErrorChar = false;
            _dcb.fInX = false;
            _dcb.fNull = false;
            _dcb.fOutX = false;
            _dcb.fOutxCtsFlow = false;
            _dcb.fOutxDsrFlow = false;
            _dcb.fTXContinueOnXoff = false;           
            _dcb.XoffChar = (sbyte) 19;
            _dcb.XonChar = (sbyte) 17;
            _dcb.XonLim = _dcb.XoffLim = (ushort) (100);

            CheckTrueReturn(_api.SetCommState(_hComHandle, _dcb),
                  "SetCommState" + _dcb.ToString());

            //CommHelpers.SetHartCommState(_hComHandle);

        }

        private void CheckTrueReturn(Boolean returnValue, string errorMessage)
        {
            
            if (!returnValue)
            {
                var lastError = new Win32Exception(Marshal.GetLastWin32Error()).Message;
                throw new InvalidOperationException(errorMessage + " Lastwin32Error:" + lastError);
            }

        }

        public void Close()
        {
            if (_hComHandle.ToInt64() == (new IntPtr(CommAPI.INVALID_HANDLE_VALUE)).ToInt64())
           {
               Log.Debug("Port Already Closed");
               return;
           }

           Log.Debug("Closing COM PORT");

            CheckTrueReturn(
          _api.SetCommMask(
          _hComHandle, // handle of communications device
           CommEventFlags.NONE)
                      ,
          "Error SetCommMask"
          );

            Thread.Sleep(300);

            DtrEnable = false;

            CheckTrueReturn(
         _api.PurgeComm(_hComHandle,
                        (uint)
                        (PurgeFlags.PURGE_RXABORT | PurgeFlags.PURGE_RXCLEAR | PurgeFlags.PURGE_TXABORT |
                         PurgeFlags.PURGE_TXCLEAR))
         , "Error PurgeComm");

            if (txOverlapped != IntPtr.Zero)
            {
                LocalFree(txOverlapped);
                txOverlapped = IntPtr.Zero;
            }
            if (rxOverlapped != IntPtr.Zero)
            {
                LocalFree(rxOverlapped);
                rxOverlapped = IntPtr.Zero;
            }


            if (_tDataReceived != null)
            {
                if (_tDataReceived.IsAlive)
                {
                    Log.Debug("Aborting Read Thread");
                    _tDataReceived.Abort();
                }
                _tDataReceived = null;
            }

            _api.CloseHandle(_hComHandle);

            _hComHandle = new IntPtr(CommAPI.INVALID_HANDLE_VALUE);

        } 
        #endregion

        #region Read And Write

        public int Read(byte[] buffer, int offset, int count)
        {

           
            var bytesToRead = BytesToRead;

            var temp = new byte[count];

            int cbRead = 0;
            bool fRead = true;
            count = Math.Min(count, (int)bytesToRead);

            if (count > 0)
            {
                fRead = _api.ReadFile(_hComHandle, temp, count, ref cbRead, rxOverlapped);
            }

            if (!fRead)
            {
                int lastError = Marshal.GetLastWin32Error();
                string errorMessage = new Win32Exception(Marshal.GetLastWin32Error()).Message;
            }

            Array.Copy(temp, 0, buffer, offset, cbRead);

            return cbRead;
        }

        public void Write(byte[] buffer, int offset, int count)
        {
                int cbWritten = 0;
                var tempBuffer = new byte[count];
                Array.Copy(buffer, offset, tempBuffer, 0, count);
                _api.WriteFile(_hComHandle, tempBuffer, count, ref cbWritten, txOverlapped);
               
        }

        private void WaitIfCtsOn()
        {
            if (_ctsON)
            {
                Log.Debug("Wait CTS to Turn OFF");
                
                int i = 0;
                while (i < 100)
                {
                    Thread.Sleep(1);
                    i++;
                    if (!_ctsON)
                    {
                        break;
                    }
                }

                Log.DebugFormat("CTS:{0}", _ctsON);

            }
        }

        private void WaitRLSD()
        {
            if (_rlsdOn)
            {
                Log.Debug("Wait RLSD to Turn OFF");

                int i = 0;
                while (i < 100)
                {
                    Thread.Sleep(1);
                    i++;
                    if (!_rlsdOn)
                    {
                        break;
                    }
                }

                Log.DebugFormat("RLSD:{0}", _rlsdOn);

            }
        }

        public int BytesToRead
        {
            get
            {
                CommErrorFlags errorFlags = CommErrorFlags.MODE;
                var commStat = new CommStat();
                _api.ClearCommError(_hComHandle, ref errorFlags, commStat);
                return (int)commStat.cbInQue;
            }
        }

        #endregion

        #region Control Signals

        private bool _dtrEnabled;

        public bool DtrEnable
        {
            get { return _dtrEnabled; }
            set
            {
                if (_hComHandle.ToInt32() == new IntPtr(CommAPI.INVALID_HANDLE_VALUE).ToInt32())
                {
                    return;
                }

                if (value)
                {
                    lock (this)
                    {
                        CheckTrueReturn(
                  _api.EscapeCommFunction(_hComHandle, CommEscapes.SETDTR),
                  "EscapeCommFunction(_hComHandle, CommEscapes.SETDTR) ");
                        _dtrEnabled = true;
                    }

                }
                else
                {
                    lock (this)
                    {
                        CheckTrueReturn(
                            _api.EscapeCommFunction(_hComHandle, CommEscapes.CLRDTR),
                            "EscapeCommFunction(_hComHandle, CommEscapes.CLRDTR) ");
                        _dtrEnabled = false;
                    }
                }

            }
        }

        public bool RtsEnable
        {
            get { throw new NotImplementedException(); }
            set
            {
                if (_hComHandle.ToInt32() == new IntPtr(CommAPI.INVALID_HANDLE_VALUE).ToInt32())
                {
                    return;
                }

                if (value)
                {
                    CheckTrueReturn(
                _api.EscapeCommFunction(_hComHandle, CommEscapes.SETRTS),
                "EscapeCommFunction(_hComHandle, CommEscapes.SETRTS) ");

                }
                else
                {
                    CheckTrueReturn(
              _api.EscapeCommFunction(_hComHandle, CommEscapes.CLRRTS),
              "EscapeCommFunction(_hComHandle, CommEscapes.CLRRTS) ");
                }

            }
        }

        public bool CtsHolding
        {
            get { throw new NotImplementedException(); }
        }

        public bool CDHolding
        {
            get { throw new NotImplementedException(); }
        }

        public string PortName
        {
            get { return _comPort; }
            set { throw new NotImplementedException(); }
        }



        #endregion

        #region Read Thread
        
        private Thread _tDataReceived;

        [DllImport("kernel32.dll")]
        static extern bool ClearCommBreak(IntPtr hFile);
        
        void DataReceivedWait()
        {
            Log.Debug("DataReceivedWait Started");

            CommEventFlags eventFlags = CommEventFlags.NONE;

            SetEventsToWait();
            bool comPortClosing = false;

            Log.DebugFormat("Waiting Events");

            while (!comPortClosing && _api.WaitCommEvent(_hComHandle, ref eventFlags))
            {
                Log.DebugFormat("Event Happened:{0}", eventFlags.ToString());


                if (eventFlags == CommEventFlags.NONE)
                {
                    comPortClosing = true;
                }

                SetEventsToWait();

                if ((eventFlags & CommEventFlags.BREAK) == CommEventFlags.BREAK)
                {
                    bool resp = ClearCommBreak(_hComHandle); 
                    Thread.Sleep(1);                    
                }

                if ((eventFlags & CommEventFlags.ERR) == CommEventFlags.ERR)
                {
                    eventFlags = CommEventFlags.NONE;
                    ClearError();
                }

                
                if ((eventFlags & CommEventFlags.CTS) == CommEventFlags.CTS)
                {
                    OnEventCts();
                }

                if ((eventFlags & CommEventFlags.RLSD) == CommEventFlags.RLSD)
                {
                    OnEventRlsd();
                }

                if ((eventFlags & CommEventFlags.RXCHAR) == CommEventFlags.RXCHAR)
                {
                    if (_dataReceivedEvent != null)
                    {
                        if (BytesToRead>0)
                        {
                            _dataReceivedEvent(this, null); 
                        }
                       
                    }
                }

                Log.DebugFormat("Waiting Events");
            }

            Log.Debug("DataReceivedWait Ended");

        }

        private void ClearError()
        {
            Thread.Sleep(100);            
            CommErrorFlags errorFlags = CommErrorFlags.MODE;
            var commStat = new CommStat();
            _api.ClearCommError(_hComHandle, ref errorFlags, commStat);
            Log.ErrorFormat("Com Error {0}", errorFlags.ToString());            
            CheckTrueReturn(
                _api.PurgeComm(_hComHandle,
                               (uint)
                               (PurgeFlags.PURGE_RXABORT | PurgeFlags.PURGE_RXCLEAR))
                , "Error PurgeComm");
        }

        private bool _ctsON = false;

        private void OnEventCts()
        {
            uint lpModemStat = 0;
            _api.GetCommModemStatus(_hComHandle, ref lpModemStat);
            if ((lpModemStat & (uint)CommModemStatusFlags.MS_CTS_ON) > 0)
            {
                Log.Debug("CTS OFF->ON");
                _ctsON = true;
            }
            else
            {
                if (BytesToRead>0)
                {
                    if (_dataReceivedEvent != null)
                    {
                        _dataReceivedEvent(this, null);
                    }  
                }
                _ctsON = false;
                Log.Debug("CTS ON->OFF");
                Log.DebugFormat("Bytes To Read:{0}", BytesToRead);
            }
        }

        private bool _rlsdOn = false;

        private void OnEventRlsd()
        {
            uint lpModemStat = 0;
            _api.GetCommModemStatus(_hComHandle, ref lpModemStat);
            if ((lpModemStat & (uint)CommModemStatusFlags.MS_RLSD_ON) > 0)
            {
                Log.Debug("RLSD OFF->ON");
                _rlsdOn = true;
            }
            else
            {
                _rlsdOn = false;
                Log.Debug("RLSD ON->OFF");
            }
        }


        private void SetEventsToWait()
        {
            _api.SetCommMask(_hComHandle,
                             CommEventFlags.TXEMPTY
                             | CommEventFlags.RXCHAR
                             | CommEventFlags.CTS
                             | CommEventFlags.BREAK
                             | CommEventFlags.RLSD
                             | CommEventFlags.ERR
                );
        } 
        
        #endregion

        #region Events 
        public event SerialDataReceivedEventHandler DataReceived
        {
            add
            {
                _dataReceivedEvent += value;

            }
            remove
            {
                if (_dataReceivedEvent != null)
                {
                    _dataReceivedEvent -= value;
                }
            }
        }


        public event SerialPinChangedEventHandler PinChanged
        {
            add
            {

            }
            remove
            {

            }
        } 
        #endregion

        #region WinAPI

        [DllImport("kernel32", EntryPoint = "LocalAlloc", SetLastError = true)]
        internal static extern IntPtr LocalAlloc(int uFlags, int uBytes);

        [DllImport("kernel32", EntryPoint = "LocalFree", SetLastError = true)]
        internal static extern IntPtr LocalFree(IntPtr hMem); 

        #endregion
    }
}
