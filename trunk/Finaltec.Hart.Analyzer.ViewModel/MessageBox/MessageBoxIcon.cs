﻿namespace Finaltec.Hart.Analyzer.ViewModel.MessageBox
{
    /// <summary>
    /// MessageBoxIcon enum.
    /// </summary>
    public enum MessageBoxIcon
    {
        None,
        Error,
        Warning,
        Question,
        Information
    }
}