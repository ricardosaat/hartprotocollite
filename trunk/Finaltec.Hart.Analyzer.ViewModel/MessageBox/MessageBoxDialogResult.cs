﻿namespace Finaltec.Hart.Analyzer.ViewModel.MessageBox
{
    /// <summary>
    /// MessageBoxDialogResult enum.
    /// </summary>
    public enum MessageBoxDialogResult
    {
        Null,
        Ok,
        Yes,
        No,
        Cancel
    }
}